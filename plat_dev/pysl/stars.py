import random

stars = ['★', '⋆', '*']


def draw_stars(screen, space):
    yx = screen.getmaxyx()

    # Draw stars, if space flag is given
    if space:
        # Set the density of stars based on terminal size
        for _ in range(yx[0] * yx[1] // 256):
            # Randomly select coordinates
            x = random.randint(2, yx[1] - 2)
            y = random.randint(2, yx[0] - 2)

            # Randomly choose a star to draw from the stars array
            screen.addstr(y, x, random.choice(stars))
