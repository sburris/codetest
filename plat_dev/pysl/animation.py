import curses
from pysl.stars import draw_stars
from datetime import datetime

# Time between frames (ms)
speed = 20


# Print safely, preventing curses errors
def safeprint(screen, y, x, string):
    yx = screen.getmaxyx()

    if y < 0 or y > yx[0] or x >= yx[1]:
        # Do nothing if result would be invisible
        return
    elif len(string) + x > yx[1]:
        # Coming on-screen
        screen.addstr(y, x, string[:x])
        # Covers up art that has wrapped around to the wrong side
        screen.addstr(y, 0, " " * len(string))
    elif x < 0:
        # Going off-screen
        screen.addstr(y, 0, string[-x:yx[1]])
    else:
        # Traveling in center
        screen.addstr(y, x, string)


# Animate art crossing the screen
def animate(art, space, name):
    # Initialize curses and hide the flashing cursor
    screen = curses.initscr()
    curses.curs_set(0)

    # Coupling rod for train animation
    coupling_rod = 'O=====O=====O=====O'

    yx = screen.getmaxyx()
    vcenter = (yx[0] - len(art)) // 2  # Center line to animate train along

    longest = len(max(art, key=len))  # Longest line in ascii art file

    for x in range(yx[1] - 1, -longest, -1):
        screen.clear()

        draw_stars(screen, space)

        for y, line in enumerate(art):
            safeprint(screen, vcenter + y, x, line)

        if name == 'train':
            # If train is chosen, animate coupling rod
            if x < yx[1] - 30:
                if x % 4 == 0:
                    safeprint(screen, vcenter + 13, x + 12, coupling_rod)
                elif x % 4 == 1:
                    safeprint(screen, vcenter + 14, x + 10, coupling_rod)
                elif x % 4 == 2:
                    safeprint(screen, vcenter + 15, x + 12, coupling_rod)
                elif x % 4 == 3:
                    safeprint(screen, vcenter + 14, x + 14, coupling_rod)

            # Animate smoke
            if datetime.now().second % 2 == 0:
                safeprint(screen, vcenter + 5, x + 9, "@@@")
                safeprint(screen, vcenter + 2, x + 13, "@@@@")
                safeprint(screen, vcenter, x + 23, "@@")
            else:
                safeprint(screen, vcenter + 3, x + 11, "@@@@")
                safeprint(screen, vcenter + 1, x + 17, "@@@")
                safeprint(screen, vcenter, x + 28, "@@")

        # Refresh screen to show new position every 20 ms
        screen.refresh()
        curses.napms(speed)
    curses.endwin()
