import click
import signal
from os import listdir, getcwd, path
from getpass import getuser
import socket
from pysl.animation import animate
import random
import pathlib

# Get a list of ascii art file names
art_dir = pathlib.Path(__file__).parent / 'art'
all_art = list(map(lambda s: s.replace('.txt', ''), listdir(art_dir)))


# Printers numbered list of options for ASCII art
def list_art():
    for i, x in enumerate(all_art):
        print(f'{i}. {x}')


# Returns fake bash prompt followed by list of files in current directory
def get_ls_output():
    output = ["{}@{} {} $ ls".format(getuser(), socket.gethostname(), path.basename(getcwd()))]
    output += listdir(getcwd())

    return output


# Creates command-line interface, help page, and arguments
@click.command()
@click.option('--number', '-n', default=None, type=click.INT, help='Index of text file in art directory to animate.')
@click.option('--file', '-f', default=None, type=click.Path(exists=True), help='Path of text file to get ASCII art from.')
@click.option('--ls', '-l', is_flag=True, help='Summon the unholy marriage of sl and ls. Overrides --name and --number.')
@click.option('--space', '-s', is_flag=True, help='Fly through space at the speed of light.')
@click.option('--name', default=random.choice(all_art), help='Name of text file in art directory to animate.')
@click.option('--listart', is_flag=True, help='Print list of possible text files to animate.')
def main(name, file, number, ls, listart, space):
    # If --art option is passed, display enumerate list of options then exit program
    if listart:
        list_art()
        quit()

    art = None

    if number:
        # Set art name via index if --number or -n is passed
        name = all_art[number]
    elif file:
        # Read file passed via --file otherwise
        with open(file, 'r') as fileart:
            art = fileart.read().split('\n')
        name = ""

    if ls:
        # If ls flag is given, the "art" is the emulated output of normal ls
        art = get_ls_output()
        name = None
    elif not art:
        # Load art from specified file name into an array containing each line
        with open(f'{ art_dir }/{ name }.txt', 'r') as fileart:
            art = fileart.read().split('\n')

    # Normalize line length by padding to match length of longest line
    longest = len(max(art, key=len))
    art = list(map(lambda l: l.ljust(longest)[:longest], art))

    animate(art, space, name)


def noop(_signum, _frame):
    pass


if __name__ == '__main__':
    # Trap SIGINT to prevent Ctrl-C
    signal.signal(signal.SIGINT, noop)
    main()
