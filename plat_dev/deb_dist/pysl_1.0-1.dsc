Format: 3.0 (quilt)
Source: pysl
Binary: python3-pysl
Architecture: all
Version: 1.0-1
Maintainer: Spencer Burris <sburris@posteo.net>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-pysl deb python optional arch=all
Checksums-Sha1:
 589bdba7f61a007425623e11ceb75e1c553cecb1 7109 pysl_1.0.orig.tar.gz
 a39fdedabf41b8cd0fb2d12e352ae856baa38158 884 pysl_1.0-1.debian.tar.xz
Checksums-Sha256:
 e359431e03cebf790146dbbf3d96fe349ccaaee6ba89330a407aed56aa4f0a49 7109 pysl_1.0.orig.tar.gz
 2c2f58263349610a216469fbd01e9fab146c283d8b8695f6661faee8c0e9fb97 884 pysl_1.0-1.debian.tar.xz
Files:
 e2520096f6cda69edd17d0c3c2b984ee 7109 pysl_1.0.orig.tar.gz
 b51f7329821604883cf1339c4abaa72e 884 pysl_1.0-1.debian.tar.xz
