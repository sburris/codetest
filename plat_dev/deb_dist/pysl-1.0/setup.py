from setuptools import setup, find_packages

setup(name='pysl',
        version='1.0',
        description='sl implemented in Python',
        author='Spencer Burris',
        author_email='sburris@posteo.net',
        packages=find_packages(),
        entry_points={
            'console_scripts': [ 'pysl=pysl.main:main']
            },
        package_data={
            '': ['pysl/art/*.txt'],
        },
        include_package_data=True,
        )
